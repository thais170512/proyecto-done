﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DONE.Startup))]
namespace DONE
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
