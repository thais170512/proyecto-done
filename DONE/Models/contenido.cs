﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DONE.Models
{
    public class contenido
    {
        public tcliente tcliente { get; set; }
        public tempresa tempresa { get; set; }
        public templeado templeado { get; set; }
        public List<tcliente> tclientes { get; set; }
        public List<tempresa>  tempresas{ get; set; }
        public List<templeado> templeados { get; set; }
    }
}