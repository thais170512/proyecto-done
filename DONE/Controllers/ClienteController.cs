﻿using DONE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;



namespace DONE.Controllers
{
    public class ClienteController : Controller
    {
        GVALORContext db = new GVALORContext();
        public ActionResult Carganomina()
        {
            var list = db.tcliente.ToList();
            list.Add(new tcliente { idcliente = 0, nombre = "[Seleccione un Cliente]" });
            list = list.OrderBy(c => c.nombre).ToList();
            ViewBag.idcliente = new SelectList(list, "idcliente", "nombre", "nombre");
            ViewBag.Error = "Debe seleccionar un cliente";
            return View();
        }
        public ActionResult Modal(int id, String mensaje, String titulo)
        {
            ViewBag.idcliente = new SelectList(db.tcliente, "idcliente", "nombre", "nombre");
            ViewBag.Error = "Debe seleccionar un cliente";
            ViewData["id"] = id;
            ViewData["mensaje"] = mensaje;
            ViewData["titulo"] = titulo;
            return View("carganomina");
        }
        public FileResult Download()
        {
            // Obtener contenido del archivo
            var ruta = Server.MapPath("~/Descargas/Formato_nomina.xlsx");
            return File(ruta, "application/vnd.ms-excel", "Formato_nomina.xlsx");
        }
        public JsonResult llenarListadoEmpleados()
        {

            int a = (int)(Session["id_cliente"]);
            int x = a;
            var lista = from c in db.templeado
                        where c.idcliente == x
                        select new
                        {
                            Codigo = c.idempleado,
                            Nombre = c.nombre + " " + c.apellidopaterno
                        };

            return Json(lista);

        }
        public ActionResult AsignarSueldosEditar(double porcentaje, int idempleado)
        {
            double valor = porcentaje / 100;
            
            var empleado = db.templeado.Find(idempleado);
            empleado.sueldo_sa=empleado.sueldototal*valor;
            empleado.sueldo_asimilado = empleado.sueldototal-(empleado.sueldototal * valor);

            return View();
            
        }
        public JsonResult EditarPorcentaje(List<ModelEmpleados> ListJson, double NuevoPorcentaje)
        {
            
            foreach (var item in ListJson)
            {
                var lista = from c in db.templeado
                            where c.idempleado == item.codigo
                            select c;
                foreach (templeado c in lista)
                {
                    c.porc_sa = NuevoPorcentaje;
                    AsignarSueldosEditar(NuevoPorcentaje,c.idempleado);
                    
                }
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }

            return Json(ListJson);
        }
        public ActionResult Lista_Empleados()
        {
            int a = (int)(Session["id_cliente"]);
            int x = a;
            List<templeado> empleado = new List<templeado>();


            var lista_empleados = from c in db.templeado
                                  where c.idcliente == x
                                  select new
                                  {
                                      c.nombre,
                                      c.idempleado,
                                      c.apellidomaterno,
                                      c.apellidopaterno,
                                      c.sueldo_sindicato,
                                      c.sueldo_sa,
                                      c.sueldo_asimilado,
                                      c.porc_sa
                                  };

            foreach (var item in lista_empleados)
            {
                templeado auxi = new templeado();
                auxi.nombre = item.nombre + " " + item.apellidopaterno + " " + item.apellidomaterno;
                auxi.idempleado = item.idempleado;
                auxi.sueldo_asimilado = item.sueldo_asimilado;
                auxi.sueldo_sa = item.sueldo_sa;
                auxi.sueldo_sindicato = item.sueldo_sindicato;
                auxi.porc_sa = item.porc_sa;
                empleado.Add(auxi);

            }

            ViewBag.lista_empleados = empleado;
            return View();
        }
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase excelfile, int idcliente)
        {

            Session["id_cliente"] = idcliente;
            string nombre_cliente = null;

            if (excelfile == null || excelfile.ContentLength == 0)
            {
                ViewBag.Error = "Debe Seleccionar un archivo <br>";
                return RedirectToAction("Modal", new { id = 1, mensaje = "Todos los campos son obligatorios", titulo = "¡Error!" });
            }
            else
            {
                ViewBag.idcliente = new SelectList(db.tcliente, "idcliente", "nombre", "nombre");
                if (excelfile.FileName.EndsWith("xls") || excelfile.FileName.EndsWith("xlsx"))
                {
                    var lista = from c in db.tcliente
                                where c.idcliente == idcliente
                                select c;
                    foreach (tcliente c in lista)
                    {
                        nombre_cliente = c.nombre;
                    }

                    string nombreArchivo = nombre_cliente + "-" + excelfile.FileName;
                    string path = Server.MapPath("~/Nominas/" + nombre_cliente + "-" + excelfile.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);

                    excelfile.SaveAs(path);

                    String[] separa;
                    String nombreEnviar;
                    separa = nombreArchivo.Split('.');

                    nombreEnviar = separa[0];

                    int retorna;
                  
                    retorna=db.sp_lee_excel_pru(idcliente.ToString(), nombreEnviar);


                    if (retorna == 0)
                    {

                        return RedirectToAction("Modal", new { id = 1, mensaje = "Compruebe que el archivo que intenta cargar tiene la estructura correcta.", titulo = "¡Carga de nómina no completada!" });
                    }
                    else
                    {

                        AsignarSueldos();
                        return RedirectToAction("Modal", new { id = 2, titulo = "¡Carga de nómina completada con éxito!" });

                    }
                }
                return View();
            }
        }
      
        public JsonResult ValidarExtension(string nombre)
        {
            JsonResponse Respuesta = new JsonResponse();

            if (nombre.IndexOf(".xls") < 0 || nombre.IndexOf(".xlsx") < 0)
            {
                Respuesta.bandera = false;
                Respuesta.texto = "Solo archivos excel! (.xls/.xlsx)";
            }
            else
            {
                Respuesta.bandera = true;
            }

            return Json(Respuesta);
        }
        public ActionResult AsignarSueldos()
        {
            double valor;
            int a = (int)(Session["id_cliente"]);
            int x = a;
            
            var lista_empleados = from c in db.templeado
                                  where c.idcliente == x
                                  select new
                                  {
                                      c.idempleado,
                                      c.sueldo_asimilado,
                                      c.sueldo_sindicato,
                                      c.sueldo_sa,
                                      c.sueldototal,
                                      c.porc_sa
                                  };

            foreach (var item in lista_empleados)
            {
                var empleado = db.templeado.Find(item.idempleado);
                valor = (double)item.porc_sa / 100;
                empleado.sueldo_sa = empleado.sueldototal * valor;
                empleado.sueldo_asimilado = empleado.sueldototal - (empleado.sueldototal * valor);
               

            }
            try
            {
                db.SaveChanges();
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return View();
        }
    }
}
