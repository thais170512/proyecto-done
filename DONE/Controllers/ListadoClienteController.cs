﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DONE.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace DONE.Controllers
{
    public class ListadoClienteController : Controller
    {
         GVALORContext db = new GVALORContext();

        // GET: /ListadoCliente/
        public ActionResult ListarClientes()
        {

            return View(db.tcliente.ToList());

        }
        public ActionResult ListarEmpleadoCliente(int id)
        {

            ViewBag.buscaridCliente = id;

            return View(db.templeado.ToList());

        }

        // GET: /ListadoCliente/Details/5
        public ActionResult DetalleEmpleado(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            templeado templeado = db.templeado.Find(id);
            if (templeado == null)
            {
                return HttpNotFound();
            }
            return View(templeado);
        }

       



        // GET: /ListadoCliente/Edit/5
        public ActionResult EditarEmpleado(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            templeado templeado = db.templeado.Find(id);
            if (templeado == null)
            {
                return HttpNotFound();
            }
            ViewBag.idcliente = new SelectList(db.tcliente, "idcliente", "nombre", templeado.idcliente);
            return View(templeado);
        }

        
        [HttpPost]
       
        public ActionResult EditarEmpleado([Bind(Include = "idempleado,iddepartamento,puesto,idtipoperiodo,turno,codigoempleado,nombre,fotografia,apellidopaterno,apellidomaterno,nombrelargo,fechanacimiento,lugarnacimiento,estadocivil,sexo,curpi,curpf,numerosegurosocial,umf,rfc,homoclave,cuentapagoelectronico,sucursalpagoelectronico,bancopagoelectronico,estadoempleado,sueldodiario,fechasueldodiario,sueldovariable,fechasueldovariable,sueldopromedio,fechasueldopromedio,sueldointegrado,fechasueldointegrado,calculado,afectado,calculadoextraordinario,afectadoextraordinario,interfazcheqpaqw,modificacionneto,fechaalta,cuentacw,tipocontrato,basecotizacionimss,tipoempleado,basepago,formapago,zonasalario,calculoptu,calculoaguinaldo,modificacionsalarioimss,altaimss,bajaimss,cambiocotizacionimss,expediente,telefono,codigopostal,direccion,poblacion,estado,nombrepadre,nombremadre,numeroafore,fechabaja,causabaja,sueldobaseliquidacion,campoextra1,campoextra2,campoextra3,fechareingreso,ajustealneto,timestamp,cidregistropatronal,ccampoextranumerico1,ccampoextranumerico2,ccampoextranumerico3,ccampoextranumerico4,ccampoextranumerico5,cestadoempleadoperiodo,cfechasueldomixto,csueldomixto,NumeroFonacot,CorreoElectronico,TipoRegimen,ClabeInterbancaria,EntidadFederativa,idcliente,idempresa,sindicato,asimilado,sa,idsindicato,idasimilado,sueldo_sindicato,sueldo_asimilado,sueldo_sa")] templeado templeado)
        {


            if (ModelState.IsValid)
            {
                db.Entry(templeado).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Debug.WriteLine("Property: {0} Error: {1}",
                                       validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
                return RedirectToAction("ListarEmpleadoCliente", "ListadoCliente", new {@id= templeado.idcliente });
            }
           

          
            ViewBag.idcliente = new SelectList(db.tcliente, "idcliente", "nombre", templeado.idcliente);

            return View(templeado);
        }

     

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
